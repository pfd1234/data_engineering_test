# Solución Prueba Técnica - Pablo Fernández

### Información Adicional:
	- Stack: Spark / PySpark / SQLSpark
	- IDE: PyCharm
	- Source: Python 3.7
	- Main Libs: Spark, PySpark 
  
### Datos de prueba: 
	- JSON File:

	{"user_id": "usuario1","date": "2021-08-03","product_name": "producto1","price": 10.00,"purchased": false}
	{"user_id": "usuario2","date": "2021-08-03","product_name": "producto3","price": 5.50,"purchased": true}
	{"user_id": "usuario3","date": "2021-08-04","product_name": "producto1","price": 10.00,"purchased": true}
	...



### Ejecución del Main:
	- Resultado princial:
	
	+--------+----------+------------+-----+
	| user_id|      date|product_name|price|
	+--------+----------+------------+-----+
	|usuario3|2021-08-04|   producto1| 10.0|
	|usuario3|2021-08-04|   producto2|  5.5|
	|usuario3|2021-09-06|   producto5| 45.9|
	|usuario3|2021-09-06|   producto6| 15.9|
	+--------+----------+------------+-----+
	
	
	- Puntos extra:
	
	Para el mes 8-2021 no hay mes previo con el que comparar
	Para el mes 9-2021 se produjo una variación en ventas de : 4.758333333333335
	Para el mes 10-2021 se produjo una variación en ventas de : -0.158333333333335

	+----+------------------+
	|year|        avg(price)|
	+----+------------------+
	|2021|17.504761904761903|
	+----+------------------+
	
	



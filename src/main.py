import pyspark
from pyspark.sql import SparkSession
from pyspark.sql.types import StructType,StructField, StringType, DateType, DoubleType, BooleanType
from pyspark.sql.functions import year, month, dayofweek, col

# SparkSession
spark = SparkSession.builder.master("local[1]").appName('PruebaTheCocktail').getOrCreate()

def create_schema():
    # Creating DF schema to read JSON files faster
    schema = StructType([
        StructField("user_id", StringType(), False),
        StructField("date", DateType(), False),
        StructField("product_name", StringType(), False),
        StructField("price", DoubleType(), False),
        StructField("purchased", BooleanType(), False)
    ])
    return schema

def expand_date(df):
    # Expanding date column for further analysis
    df = df.withColumn('month', month(col('date')))
    df = df.withColumn('year', year(col('date')))
    return df

def user_purch(df, usuario):
    # Checking a user purchases
    df = df.select(col("user_id"), col("date"), col("product_name"), col("price")) \
        .filter((col("purchased") == True) & (col("user_id") == usuario)).sort("date")
    return df

def avg_month_sales(df, year):
    # Checking the average sales per month
    df = df.filter((col("purchased") == True) & (col("year") == year)).\
        groupBy("year", "month").avg("price").sort("month")
    return df

def print_variation_month_sales(df):
    # Printing the variation of sales with the previous month
    # List of AVG SALES to compare
    avgList = [row[0] for row in df_sales_per_month.select("avg(price)").collect()]
    avgList2 = avgList

    # Loop though AVG sales to obtain the variation
    res = []
    i = 0

    for row in avgList:
        if i == 0:
            res.append(0.0)
            i += 1
        else:
            res.append(row - avgList2[i - 1])
            i += 1
    j = 0
    dataCollect = df_sales_per_month.collect()
    for row in dataCollect:
        val = res[j]
        if j == 0:
            print("Para el mes " + str(row['month']) + '-' + str(row['year']) + ' no hay mes previo con el que comparar')
            j += 1
        else:
            print("Para el mes " + str(row['month']) + '-' + str(row['year']) + ' se produjo una variación en ventas de : ' + str(val))
            j += 1

def avg_annual_sales(df):
    # Obtaining the average of sales per year
    return df.groupBy("year").avg("price").sort("avg(price)")

if __name__ == '__main__':
    # Reading JSON to DF with Schema
    schema = create_schema()
    df = spark.read.schema(schema) \
        .json("visitas.json")

    # Expanding date for filtering purposes
    df = expand_date(df)

    ## 1.- Checking all the PURCHASES made by a USER
    ### usuario3
    df_u1 = user_purch(df,"usuario3")
    df_u1.show()

    '''
        EXTRA POINTS
    '''

    ## 2.1- Checking the AVERAGE SALES per MONTH
    df_sales_per_month = avg_month_sales(df,2021)
        #df_sales_per_month.show()

    ## 2.2- Printing the VARIATION SALES per MONTH (compared with the previous MONTH)
    print_variation_month_sales(df_sales_per_month)

    ## 3.- Average Annual Sales
    dfAvgAnnual = avg_annual_sales(df)
    dfAvgAnnual.show()